var progress;
var progress_text;
var chronometer;
var timer;
var notification_text;
var title;

function time_in_title(time){
    title.text(time + " - Tomate: Timer para Pomodor technique");
}
function notify() {
    if (Notification && Notification.permission == 'granted'){
        new Notification('Tomate', {
            body: notification_text,
            icon: 'images/tomate_icon.jpg'
        });
    }
    document.getElementById('alarm').play();
}
function two_digits(number) {
    if (number.toString().length < 2) {
        return '0' + number.toString();
    }
    return number;
}
function chrono(seconds, minutes){
    var time = two_digits(minutes) + ':' + two_digits(seconds);
    time_in_title(time);
    chronometer.text(time);
}
function seconds_to_pair(seconds){
    var minutes = Math.floor(seconds / 60);
    seconds = seconds - minutes * 60;
    return [seconds, minutes];
}
function set_time(total_seconds){
    var pair = seconds_to_pair(total_seconds);
    progress.attr('aria-valuenow', '0');
    progress.attr('style', 'width: 0%');
    progress_text.text(0 + ' percent');
    chrono(pair[0], pair[1]);
}
function update_time(total_seconds, time_start, time_now){
    var seconds = Math.floor((time_now - time_start) / 1000);
    var percentage = Math.ceil(seconds * 100 / total_seconds);
    var pair = seconds_to_pair(total_seconds - seconds);
    progress.attr('aria-valuenow', percentage);
    progress.attr('style', 'width: ' + percentage + '%');
    progress_text.text(percentage + ' percent');
    chrono(pair[0], pair[1]);
}

function start(total_seconds){
    var time_start = Date.now();
    var time_end = time_start + total_seconds * 1000;
    var time_last = time_start;
    reset();
    set_time(total_seconds);
    timer = setInterval(function (){
        var now = Date.now();
        if (now >= time_end) {
            clearInterval(timer);
            update_time(total_seconds, time_start, time_end); // Avoid roundings
            notify();
            return;
        }
        if (now - time_last >= 1000){
            update_time(total_seconds, time_start, now);
            time_last = now;
        }

    }, 100);
}

function reset(){
    set_time(0);
    clearInterval(timer);
}

$(function (){
    $('#pomodoro').click(function (event){
        start(25 * 60);
    });
    $('#break').click(function (event){
        start(5 * 60);
    });
    $('#long_break').click(function (event){
        var index = $('#long-break-time').prop('selectedIndex') + 2;
        start(index * 5 * 60);
    });
    $('#reset').click(function (event){
        reset();
    });
    title = $('html > head > title');
    progress = $('#progress');
    progress_text = $('#progress > span');
    chronometer = $('#chronometer');
    var p = document.createElement('p');
    p.innerHTML = 'Se acab&oacute; el tiempo';
    notification_text = p.textContent;
    reset();
    if (Notification && Notification.permission != 'granted') {
        Notification.requestPermission();
    }
});
